/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ageOfPregnancy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author KA335933
 */
public class Pregnancy {

    public static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", new Locale("pl_PL"));

    private static Pregnancy INSTANCE;

    private static Calendar dateOfConception = null;
    private static Calendar dateOfBirth = null;
    private static final Calendar currentDate = Calendar.getInstance();
    private static int daysBetween = 0;
//    private static Integer menstrualPeriod = null;

    private Pregnancy() {
    } //blokuje uzycie konstruktora

    public static Pregnancy getInstanceUsingDateOfTheLastMenstrualPeriod(Calendar dateOfTheLastMenstrualPeriod, int menstrualPeriod) throws Exception {
        if (dateOfTheLastMenstrualPeriod != null) {
            if (menstrualPeriod < 14) {
                throw new Exception("Zbyt krótki cykl menstrualny");
            }

//            menstrualPeriod = new Integer(menstrualPeriod);
            Calendar dateOfConception = (Calendar) dateOfTheLastMenstrualPeriod.clone();
            dateOfConception.add(Calendar.DATE, menstrualPeriod - 14);

            if (dateOfConception.getTime().after(new Date())) {
                throw new Exception("Dla wskazanej daty dzień poczęcia byłaby w przyszłości");
            }

            return getInstanceUsingDateOfConception(dateOfConception);
        } else {
            throw new NullPointerException();
        }
    }

    public static Pregnancy getInstanceUsingDateOfBirth(Calendar dateOfBirth) throws Exception {
        if (dateOfBirth != null) {
//            menstrualPeriod = null;
            Calendar dateOfConceptionTemp = (Calendar) dateOfBirth.clone();
            dateOfConceptionTemp.add(Calendar.DATE, -266);

            return getInstanceUsingDateOfConception(dateOfConceptionTemp);
        } else {
            throw new NullPointerException();
        }
    }

    public static Pregnancy getInstanceUsingDateOfConception(Calendar dateOfConception) throws Exception {
        if (INSTANCE == null) {
            INSTANCE = new Pregnancy();
        }

        if (dateOfConception.getTime().after(new Date())) {
            throw new Exception("Wprowadzona data jest po dacie dzisiejszej");
        }

        if (dateOfConception != null) {
//            menstrualPeriod = null;
            Pregnancy.dateOfConception = dateOfConception;
            Pregnancy.INSTANCE.currentDate.setTime(new Date());
            Pregnancy.INSTANCE.dateOfBirth = (Calendar) dateOfConception.clone();
            Pregnancy.INSTANCE.dateOfBirth.add(Calendar.DATE, 266);  // number of days to add
            Pregnancy.INSTANCE.calculateDaysBetween();

            return INSTANCE;
        } else {
            throw new NullPointerException();
        }
    }

    private void calculateDaysBetween() {
        this.daysBetween = (int) ((currentDate.getTimeInMillis() - dateOfConception.getTimeInMillis()) / (1000 * 60 * 60 * 24));
    }

    public String getFormatedDateOfConception() {
        return dateFormat.format(dateOfConception.getTime());
    }

    public String getFormatedDateOfBirth() {
        return dateFormat.format(dateOfBirth.getTime());
    }

    public String getFormatedCurrentDate() {
        return dateFormat.format(currentDate.getTime());
    }

    public String getFormatedDaysBetween() {
        return String.valueOf(daysBetween);
    }

    public int getDaysBetween() {
        return daysBetween;
    }
    
    public int getPregnancyProgressBarValue(){
        //float value = (float) Pregnancy.daysBetween / 266 * 100;
        //return (int) value;
        return Pregnancy.daysBetween;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        //----------------------------------------------------------------------
        sb.append("Dziś jest: ").append(getFormatedCurrentDate());
        sb.append("\n");
        /*if (Pregnancy.menstrualPeriod != null){
            sb.append("Długość cyklu: ").append(Pregnancy.menstrualPeriod.toString());
        }*/
        //----------------------------------------------------------------------
        sb.append("Data poczęcia: ").append(getFormatedDateOfConception());
        sb.append("\n");
        //----------------------------------------------------------------------
        sb.append("Data porodu: ").append(getFormatedDateOfBirth());
        sb.append("\n");
        //----------------------------------------------------------------------
        sb.append("Jesteś w: ").append(getDaysBetween() / 7 + 1).append(" tygodniu ciąży");
//        sb.append("\n");
        //----------------------------------------------------------------------
//        sb.append("Ginekolog powie Ci że jesteś w: ").append(getDaysBetween() / 7 + 3).append(" tygodniu ciąży");
//        sb.append("\n");
        sb.append(" (ginekologicznie w: ").append(getDaysBetween() / 7 + 3).append(" tygodniu)");
        sb.append("\n");
        //----------------------------------------------------------------------
        if (getDaysBetween() == 1) sb.append("Dziecko ma: ").append(getFormatedDaysBetween()).append(" dzień");
        else sb.append("Dziecko ma: ").append(getFormatedDaysBetween()).append(" dni");
        sb.append("\n");
        //----------------------------------------------------------------------
        sb.append("Dziecko ma: ").append(getDaysBetween() / 7);
        if (getDaysBetween() / 7 == 1) sb.append(" tydzień");
        else if (getDaysBetween() / 7 > 1 && getDaysBetween() / 7 <= 4) sb.append(" tygodnie");
        else sb.append(" tygodni");
        if (getDaysBetween() % 7 != 0)
            if (getDaysBetween()%7 == 1) sb.append(", ").append(getDaysBetween()%7).append(" dzień");
            else sb.append(", ").append(getDaysBetween() % 7).append(" dni");

        return sb.toString();
    }
}
